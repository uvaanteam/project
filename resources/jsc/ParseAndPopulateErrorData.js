 try{
     var errorPayload ="";
     var statusCode = context.getVariable("message.status.code");
     var description ="";
     var errorPhrase ="";
     var errorCode ="";
     var errorId ="";
     var errorName = context.getVariable("errorJSON");
     print("errorName.." +errorName);
     var errorJson = context.getVariable(errorName);
     var transactionId = context.getVariable("transactionId");
     var faultName = context.getVariable("fault.name");
     
     var dateForError = new Date().toString();
     if(errorJson)
     {
         errorPayload =JSON.parse(errorJson);
         print("errorPayload.." +errorPayload);
         errorPhrase = errorPayload.reasonPhrase;
         statusCode = errorPayload.statusCode;
         description = errorPayload.errorDescription;
         errorCode = errorPayload.errorCode;
         
     } else if(faultName === "invalid_access_token")
     {
         errorId ="invalid_access_token";
     }else if(faultName === "access_token_expired")
     {
         errorId ="access_token_expired";
     }else if(faultName === "SpikeArrestViolation")
     {
         errorId ="spike_arrest_violation";
     }else if(faultName === "InvalidBasicAuthenticationSource")
     {
         errorId ="invalid_authorization_header";
     }else if(faultName === "QuotaViolation")
     {
         errorId ="quota_limit_reached";
     }else if(faultName === "InvalidApiKey")
     {
         errorId ="invalid_apikey";
     }
     
     if(errorId)
     {
         errorPayload =JSON.parse(context.getVariable(errorId));
         print("errorPayload.." +errorPayload);
         errorPhrase = errorPayload.reasonPhrase;
         statusCode = errorPayload.statusCode;
         description = errorPayload.errorDescription;
         errorCode = errorPayload.errorCode;
         
     }
     context.setVariable("errorPhrase",errorPhrase);
     context.setVariable("statusCode",statusCode);
     context.setVariable("description",description);
     context.setVariable("errorCode",errorCode);
     context.setVariable("dateForError",dateForError);
     context.setVariable("transactionId",transactionId);
     
     
 }catch(err){
     throw err;
 }